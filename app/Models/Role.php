<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public $timestamps = false;
    protected $fillable = ['name'];

    public const ADMIN = 1;
    public const USER = 2;

    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
