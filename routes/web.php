<?php

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login')->name('login.attempt')->uses('Auth\LoginController@login');

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::resource('/', 'DashController');
    Route::get('/logout')->name('logout')->uses('Auth\LoginController@logout');

    Route::resource('/brands', 'BrandController');
    Route::resource('/categories', 'CategoryController');
    Route::resource('/products', 'ProductController');

//    Route::get('/brands', 'BrandController@index');
//    Route::post('/brands', 'BrandController@store');
//    Route::put('/brands/{id}', 'BrandController@update');
//    Route::delete('/brands/{id}', 'BrandController@destroy');
});

